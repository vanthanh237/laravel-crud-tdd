@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="page-header">
                    <h2>Task Detail</h2>
                </div>
                <div class="card">
                    <div class="card-header">
                        {{$task->name}}
                    </div>

                    <div class="card-body">
                        {{$task->content}}
                    </div>

                    <div class="card-footer">
                        <a href="{{ route('tasks.update', $task->id) }}" class="btn btn-warning">Update</a>
                        <form action="{{ route('tasks.destroy', $task->id) }}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button class="btn btn-danger">Delete</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
