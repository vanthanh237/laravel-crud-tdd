<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Tests\TestCase;

class UpdateTaskTest extends TestCase
{
    public function getUpdateTaskRoute()
    {
        return route('tasks.edit');
    }

    /** @test */
    public function authenticated_user_can_update_the_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $task->name = "Updated Title";
        $this->put('/tasks/edit/'.$task->id, $task->toArray());
        $this->assertDatabaseHas('tasks',['id'=> $task->id , 'name' => 'Updated Title']);
    }

    /** @test */
    public function unauthenticated_user_can_not_update_the_task()
    {
        $task = Task::factory()->create();
        $task->name = "Updated Title";
        $response = $this->put('/tasks/edit/'.$task->id, $task->toArray());
        $response->assertRedirect('/login');
    }

    /** @test */
    public  function authenticated_user_can_view_update_task_form()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->get('/tasks/update/'.$task->id);
        $response->assertViewIs('tasks.update');
    }

    /** @test */
    public function unauthenticated_user_can_not_see_update_task_form_view()
    {
        $task = Task::factory()->create();
        $response = $this->get('/tasks/update/'.$task->id);
        $response->assertRedirect('/login');
    }
}
