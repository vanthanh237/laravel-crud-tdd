<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteTaskTest extends TestCase
{
    /** @test */
    public function authorized_user_can_delete_the_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $this->delete('/tasks/'.$task->id);
        $this->assertDatabaseMissing('tasks',['id'=> $task->id]);
    }

    /** @test */
    public function unauthorized_user_cannot_delete_the_task(){
        $task = Task::factory()->create();
        $response = $this->delete('/tasks/'.$task->id);
        $response->assertRedirect('/login');
    }
}
