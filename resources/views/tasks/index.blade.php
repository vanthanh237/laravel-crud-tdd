@extends("layouts.app")

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="page-header">
                <h2>All Tasks</h2>
            </div>
            <div class="col-md-12">
                <a href="{{ route('tasks.create') }}" class="btn btn-success">Create</a>
            </div>
            <table class="table table-striped">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Content</th>
                    <th>Action</th>
                </tr>
                @foreach($tasks as $task)
                    <tr>
                        <th>{{$task->id}}</th>
                        <th>{{$task->name}}</th>
                        <th>{{$task->content}}</th>
                        <th>
                            <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-success">Detail</a>
                            <a href="{{ route('tasks.update', $task->id) }}" class="btn btn-warning">Update</a>
                            <form action="{{ route('tasks.destroy', $task->id) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </th>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
