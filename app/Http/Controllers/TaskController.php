<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTaskRequest;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    protected $task;

    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    public function index()
    {
        $tasks = $this->task->all();
        return view('tasks.index', compact('tasks'));
    }

    public function store(CreateTaskRequest $request)
    {
        $this->task->create($request->all());
        return redirect()->route('tasks.index');
    }

    public function create()
    {
        return view('tasks.create');
    }

    public function edit(Request $request, $id)
    {
        $task = $this->task->find($id);
        $task->update($request->all());
        return redirect('/tasks/show/'.$task->id);
    }

    public function update($id)
    {
        $task = $this->task->find($id);
        return view('tasks.update', compact('task'));
    }

    public function show($id)
    {
        $task = $this->task->find($id);
        return view('tasks.detail',compact('task'));
    }

    public function destroy($id)
    {
        $task = $this->task->find($id);
        $task->delete();
        return redirect("/tasks");
    }
}
