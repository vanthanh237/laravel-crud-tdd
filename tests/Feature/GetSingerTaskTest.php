<?php

namespace Tests\Feature;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetSingerTaskTest extends TestCase
{
    public function getSingerTaskRoute()
    {
        return route('tasks.show');
    }

    /** @test */
    public function a_user_can_read_singer_task()
    {
        $task = Task::factory()->create();
        $response = $this->get('/tasks/show/'.$task->id);
        $response->assertStatus(Response::HTTP_OK);

        $response->assertViewIs('tasks.detail');
        $response->assertSee($task->name)
            ->assertSee($task->content);
    }
}
