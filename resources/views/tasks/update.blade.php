@extends("layouts.app")

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2>Update Task</h2>
                <form action="{{route('tasks.edit', $task->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="card">
                        <div class="card-header">
                            <input type="text" class="form-group" name="name" placeholder="Name..." value="{{$task->name}}">
                            @error('name')
                            <span id="name-error" class="error text-danger" style="display: block">
                                {{$message}}
                            </span>
                            @enderror
                        </div>
                        <div class="card-body">
                            <input type="text" class="form-group" name="content" placeholder="Content..." value="{{$task->content}}">
                        </div>
                        <button class="btn-success">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
